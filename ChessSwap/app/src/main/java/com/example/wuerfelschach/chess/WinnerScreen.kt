package com.example.wuerfelschach.chess

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel

@Composable
fun WinnerScreen(onMainMenuClick: () -> Unit, vm : ChessboardViewModel= viewModel()) {
    Box(modifier = Modifier
        .size(180.dp)
        .background(Color.White)){
        Column(verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
            Spacer(modifier = Modifier.height(30.dp))
            Text(text = "${vm.state.chessPosition.matrix[vm.state.target!!.y][vm.state.target!!.x]?.chessSet} won", fontWeight = FontWeight.Bold)
            Spacer(modifier = Modifier.size(30.dp))
            Button(
                onClick = { onMainMenuClick() },
                modifier = Modifier.padding(16.dp)
            ) {
                Text(text = "Return to Main Menu")
            }
        }
    }

}