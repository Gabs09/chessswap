package com.example.wuerfelschach.chess

import com.example.wuerfelschach.enums.PieceType

data class ChessboardState(
    val chessPosition: ChessPosition,

    var randomPieceType: PieceType?=null,
    val piece: String="",
    var gameWon: Boolean=false,

    var target: Coordinates?=null,
    var checkedCoords: Coordinates?=null,
    val selected: Coordinates? = null,
    val whiteOnTurn: Boolean = true,
)
