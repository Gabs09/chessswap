package com.example.wuerfelschach.chess

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.wuerfelschach.R
import com.example.wuerfelschach.enums.PieceType
import com.example.wuerfelschach.enums.ChessSet
import kotlinx.coroutines.launch
import kotlin.math.abs
import kotlin.random.Random

class ChessboardViewModel : ViewModel() {
    var state by mutableStateOf(ChessboardState(ChessPosition(MutableList(8) {
        MutableList(8) { null }
    })))

    var pieceType: String
        get() = state.piece
        set(value) {
            state { copy(piece = value) }
        }
    var chessPosition: ChessPosition
        get() = state.chessPosition
        set(value) {
            state { copy(chessPosition = value) }
        }

    private fun state(update: suspend ChessboardState.() -> ChessboardState) {
        viewModelScope.launch {
            state = state.update()
        }
    }



    fun canMove(target: Coordinates) {

        return when (state.chessPosition.matrix[state.selected?.y!!][state.selected?.x!!]?.pieceType) {
            PieceType.PAWN -> canPawnMove(target)
            PieceType.BISHOP -> canBishopMove(target)
            PieceType.KING -> canKingMove(target)
            PieceType.QUEEN -> canQueenMove(target)
            PieceType.KNIGHT -> canKnightMove(target)
            PieceType.ROOK -> canRookMove(target)
            null -> TODO()
        }
    }

    private fun canRookMove(target: Coordinates) {
        val deltaY = abs(state.selected?.y!! - target.y)
        val deltaX = abs(state.selected?.x!! - target.x)
        val allowedToMoveY = isClearVertically(target)
        val allowedToMoveX = isClearHorizontally(target)
        val newPosition = state.chessPosition.matrix[target.y][target.x]

        if (deltaY != 0 && deltaX == 0 && allowedToMoveY || deltaX != 0 && deltaY == 0 && allowedToMoveX) move(
            target
        )


    }


    private fun isClearHorizontally(target: Coordinates): Boolean {
        var allowedToMoveX = true

        if (state.selected!!.x < target.x) {
            for (i in state.selected!!.x + 1..target.x) {
                if (state.chessPosition.matrix[target.y][i] == null) continue else allowedToMoveX =
                    false
            }
        }
        if (state.selected!!.x > target.x) {
            for (i in state.selected!!.x - 1 downTo target.x) {
                if (state.chessPosition.matrix[target.y][i] == null) continue else allowedToMoveX =
                    false
            }
        }
        if (state.chessPosition.matrix[target.y][target.x] != null) {
            allowedToMoveX = true
        }
        return allowedToMoveX
    }

    private fun isClearVertically(target: Coordinates): Boolean {
        var allowedToMoveY = true
        if (state.selected!!.y < target.y) {
            for (i in state.selected!!.y + 1..target.y) {
                if (state.chessPosition.matrix[i][target.x] == null) continue else allowedToMoveY =
                    false
            }
        }
        if (state.selected!!.y > target.y) {
            for (i in state.selected!!.y - 1 downTo target.y) {
                if (state.chessPosition.matrix[i][target.x] == null) continue else allowedToMoveY =
                    false
            }
        }
        if (state.chessPosition.matrix[target.y][target.x] != null) {
            allowedToMoveY = true
        }
        return allowedToMoveY
    }

    private fun canKnightMove(target: Coordinates) {
        val deltaY = abs(state.selected?.y!! - target.y)
        val deltaX = abs(state.selected?.x!! - target.x)

        if ((deltaY == 2 && deltaX == 1) || (deltaY == 1 && deltaX == 2)) move(target)

    }

    private fun canQueenMove(target: Coordinates) {
        val deltaY = abs(state.selected?.y!! - target.y)
        val deltaX = abs(state.selected?.x!! - target.x)
        val allowedToMoveY = isClearVertically(target)
        val allowedToMoveX = isClearHorizontally(target)
        val canDiagonalTopRight = canMoveDiagonalTopRight(target, deltaX)
        val canDiagonalTopLeft = canDiagonalTopLeft(target, deltaX)
        val canDiagonalBottomRight = canDiagonalBottomRight(target, deltaX)
        val canDiagonalBottomLeft = canDiagonalBottomLeft(target, deltaX)

        if ((deltaY != 0 && deltaX == 0 && allowedToMoveY || deltaX != 0 && deltaY == 0 && allowedToMoveX) || (deltaY == deltaX && canDiagonalTopRight && canDiagonalTopLeft && canDiagonalBottomLeft && canDiagonalBottomRight)) {
            move(target)
        }
    }

    private fun canKingMove(target: Coordinates) {
        val deltaY = abs(state.selected?.y!! - target.y)
        val deltaX = abs(state.selected?.x!! - target.x)

        if ((deltaY == 1 && deltaX == 1 ) ||
            (deltaY + deltaX == 1 )
        ) return move(target)

    }

    private fun canBishopMove(target: Coordinates) {
        val deltaY = abs(state.selected?.y!! - target.y)
        val deltaX = abs(state.selected?.x!! - target.x)
        val canDiagonalTopRight = canMoveDiagonalTopRight(target, deltaX)
        val canDiagonalTopLeft = canDiagonalTopLeft(target, deltaX)
        val canDiagonalBottomRight = canDiagonalBottomRight(target, deltaX)
        val canDiagonalBottomLeft = canDiagonalBottomLeft(target, deltaX)

        if ((deltaY == deltaX && canDiagonalTopRight && canDiagonalTopLeft && canDiagonalBottomLeft && canDiagonalBottomRight) /*|| (deltaY == deltaX && canDiagonalTopLeft)*/) {
            move(target)
        }

    }

    private fun canDiagonalBottomLeft(target: Coordinates, deltaX: Int): Boolean {
        var canDiagonalBottomLeft = true
        if (state.selected!!.x > target.x && state.selected!!.y > target.y) {
            for (i in 1..deltaX) {
                if (state.chessPosition.matrix[state.selected!!.y - i][state.selected!!.x - i] == null) continue else canDiagonalBottomLeft =
                    false
            }
        }
        if (state.chessPosition.matrix[target.y][target.x] != null) {
            canDiagonalBottomLeft = true
        }
        return canDiagonalBottomLeft
    }

    private fun canDiagonalBottomRight(target: Coordinates, deltaX: Int): Boolean {
        var canDiagonalBottomRight = true
        if (state.selected!!.x < target.x && state.selected!!.y > target.y) {
            for (i in 1..deltaX) {
                if (state.chessPosition.matrix[state.selected!!.y - i][state.selected!!.x + i] == null) continue else canDiagonalBottomRight =
                    false
            }
        }
        if (state.chessPosition.matrix[target.y][target.x] != null) {
            canDiagonalBottomRight = true
        }
        return canDiagonalBottomRight
    }

    private fun canDiagonalTopLeft(target: Coordinates, deltaX: Int): Boolean {
        var canDiagonalTopLeft = true
        if (state.selected!!.x > target.x && state.selected!!.y < target.y) {
            for (i in 1..deltaX) {
                if (state.chessPosition.matrix[state.selected!!.y + i][state.selected!!.x - i] == null) continue else canDiagonalTopLeft =
                    false
            }
        }
        if (state.chessPosition.matrix[target.y][target.x] != null) {
            canDiagonalTopLeft = true
        }
        return canDiagonalTopLeft
    }

    private fun canMoveDiagonalTopRight(target: Coordinates, deltaX: Int): Boolean {
        var canDiagonalTopRight = true
        if (state.selected!!.x < target.x && state.selected!!.y < target.y) {
            for (i in 1..deltaX) {
                if (state.chessPosition.matrix[state.selected!!.y + i][state.selected!!.x + i] == null) continue
                else canDiagonalTopRight = false
            }
        }
        if (state.chessPosition.matrix[target.y][target.x] != null) {
            canDiagonalTopRight = true
        }
        return canDiagonalTopRight
    }

    private fun canPawnMove(target: Coordinates) {
        val deltaY = abs(state.selected?.y!! - target.y)
        val deltaX = abs(state.selected?.x!! - target.x)
        val playerColor =
            state.chessPosition.matrix[state.selected!!.y][state.selected!!.x]?.chessSet

        if (deltaX == 0) {
            //move forward
            if ((deltaY == 1 && state.selected!!.y < target.y && state.chessPosition.matrix[state.selected!!.y][state.selected!!.x]?.chessSet == ChessSet.WHITE && state.chessPosition.matrix[target.y][target.x] == null) ||
                (deltaY == 1 && state.selected!!.y > target.y && state.chessPosition.matrix[state.selected!!.y][state.selected!!.x]?.chessSet == ChessSet.BLACK && state.chessPosition.matrix[target.y][target.x] == null) ||
                (deltaY == 2 && state.selected!!.y == 1) ||
                (deltaY == 2 && state.selected!!.y == 6)
            ) {
                move(target)
            }
        }
        if (deltaX == 1 && deltaY == 1) {
            if (state.chessPosition.matrix[target.y][target.x] != null) {
                move(target)
            }
        }
    }


    fun highlightSelected(coord: Coordinates): Boolean {
        return state.selected == coord
    }

    fun setSelected(new: Coordinates) {
        state {
            copy(selected = if (state.chessPosition.matrix[new.y][new.x] != null) new else null)
        }
    }


    fun move(target: Coordinates) {
        if (state.chessPosition.matrix[target.y][target.x] == null) {
            if (state.selected?.y == 6 && state.chessPosition.matrix[state.selected!!.y][state.selected!!.x]?.pieceType == PieceType.PAWN && target.y == 7) {
                val temp = state.chessPosition.copy()
                temp.matrix[target.y][target.x] =
                    temp.matrix[state.selected!!.y][state.selected!!.x]
                Log.d("target", state.chessPosition.matrix[target.y][target.x].toString())
                temp.matrix[target.y][target.x] =
                    Piece(ChessSet.WHITE, R.drawable.queen_light /*"Queen"*/, PieceType.QUEEN)
                temp.matrix[state.selected?.y!!][state.selected?.x!!] = null

                state {
                    copy(
                        chessPosition = temp,
                        selected = null,
                        whiteOnTurn = !state.whiteOnTurn
                    )
                }
            }
            //PROMOTION
            else if (state.selected?.y == 1 && state.chessPosition.matrix[state.selected!!.y][state.selected!!.x]?.pieceType == PieceType.PAWN && target.y == 0) {
                val temp = state.chessPosition.copy()
                temp.matrix[target.y][target.x] =
                    temp.matrix[state.selected!!.y][state.selected!!.x]
                Log.d("target", state.chessPosition.matrix[target.y][target.x].toString())
                temp.matrix[target.y][target.x] =
                    Piece(ChessSet.BLACK, R.drawable.queen_dark /*"Queen"*/, PieceType.QUEEN)
                temp.matrix[state.selected?.y!!][state.selected?.x!!] = null

                state {
                    copy(
                        chessPosition = temp,
                        selected = null,
                        whiteOnTurn = !state.whiteOnTurn
                    )
                }
            } else {
                val temp = state.chessPosition.copy()
                temp.matrix[target.y][target.x] =
                    temp.matrix[state.selected!!.y][state.selected!!.x]
                Log.d("target", state.chessPosition.matrix[target.y][target.x].toString())
                temp.matrix[state.selected?.y!!][state.selected?.x!!] = null
                state {
                    copy(
                        chessPosition = temp,
                        selected = null,
                        whiteOnTurn = !state.whiteOnTurn
                    )
                }
            }
        } else if ((state.chessPosition.matrix[target.y][target.x] != null) &&
            (state.chessPosition.matrix[state.selected!!.y][state.selected!!.x]?.chessSet != (state.chessPosition.matrix[target.y][target.x]?.chessSet))
        ) {
            if (state.chessPosition.matrix[target.y][target.x]?.pieceType == PieceType.KING) {
                val temp = state.chessPosition.copy()
                temp.matrix[target.y][target.x] =
                    temp.matrix[state.selected!!.y][state.selected!!.x]
                Log.d("target", state.chessPosition.matrix[target.y][target.x].toString())
                temp.matrix[state.selected?.y!!][state.selected?.x!!] = null
                state {
                    copy(
                        chessPosition = temp,
                        selected = null,
                        whiteOnTurn = !state.whiteOnTurn
                    )
                }
                state.target = target
                gameWin()
            } else {
                val temp = state.chessPosition.copy()
                temp.matrix[target.y][target.x] =
                    temp.matrix[state.selected!!.y][state.selected!!.x]
                Log.d("target", state.chessPosition.matrix[target.y][target.x].toString())
                temp.matrix[state.selected?.y!!][state.selected?.x!!] = null
                state {
                    copy(
                        chessPosition = temp,
                        selected = null,
                        whiteOnTurn = !state.whiteOnTurn
                    )
                }
            }
        }
    }

    fun updatePiece() {

        val randomPiece = createRandomChessPiece()

        pieceType = randomPiece.name
        val temp = chessPosition
        if (state.whiteOnTurn && chessPosition.matrix[state.selected!!.y][state.selected!!.x]?.pieceType != PieceType.KING) {
            when (randomPiece) {
                PieceType.PAWN -> temp.matrix[state.selected?.y!!][state.selected?.x!!] =
                    Piece(
                        chessSet = temp.matrix[state.selected!!.y][state.selected!!.x]?.chessSet,
                        R.drawable.pawn_light, randomPiece
                    )

                PieceType.BISHOP -> temp.matrix[state.selected?.y!!][state.selected?.x!!] =
                    Piece(
                        chessSet = temp.matrix[state.selected!!.y][state.selected!!.x]?.chessSet,
                        R.drawable.bishop_light, randomPiece
                    )

                PieceType.KING -> temp.matrix[state.selected?.y!!][state.selected?.x!!] =
                    Piece(
                        chessSet = temp.matrix[state.selected!!.y][state.selected!!.x]?.chessSet,
                        R.drawable.king_light, randomPiece
                    )

                PieceType.QUEEN -> temp.matrix[state.selected?.y!!][state.selected?.x!!] =
                    Piece(
                        chessSet = temp.matrix[state.selected!!.y][state.selected!!.x]?.chessSet,
                        R.drawable.queen_light, randomPiece
                    )

                PieceType.KNIGHT -> temp.matrix[state.selected?.y!!][state.selected?.x!!] =
                    Piece(
                        chessSet = temp.matrix[state.selected!!.y][state.selected!!.x]?.chessSet,
                        R.drawable.knight_light, randomPiece
                    )

                PieceType.ROOK -> temp.matrix[state.selected?.y!!][state.selected?.x!!] =
                    Piece(
                        chessSet = temp.matrix[state.selected!!.y][state.selected!!.x]?.chessSet,
                        R.drawable.rook_light, randomPiece
                    )
            }
        }
       if (!state.whiteOnTurn && chessPosition.matrix[state.selected!!.y][state.selected!!.x]?.pieceType != PieceType.KING)
            {
            when (randomPiece) {
                PieceType.PAWN -> state.chessPosition.matrix[state.selected?.y!!][state.selected?.x!!] =
                    Piece(
                        chessSet = state.chessPosition.matrix[state.selected!!.y][state.selected!!.x]?.chessSet,
                        R.drawable.pawn_dark, randomPiece
                    )

                PieceType.BISHOP -> state.chessPosition.matrix[state.selected?.y!!][state.selected?.x!!] =
                    Piece(
                        chessSet = state.chessPosition.matrix[state.selected!!.y][state.selected!!.x]?.chessSet,
                        R.drawable.bishop_black, randomPiece
                    )

                PieceType.KING -> state.chessPosition.matrix[state.selected?.y!!][state.selected?.x!!] =
                    Piece(
                        chessSet = state.chessPosition.matrix[state.selected!!.y][state.selected!!.x]?.chessSet,
                        R.drawable.king_dark, randomPiece
                    )

                PieceType.QUEEN -> state.chessPosition.matrix[state.selected?.y!!][state.selected?.x!!] =
                    Piece(
                        chessSet = state.chessPosition.matrix[state.selected!!.y][state.selected!!.x]?.chessSet,
                        R.drawable.queen_dark, randomPiece
                    )

                PieceType.KNIGHT -> state.chessPosition.matrix[state.selected?.y!!][state.selected?.x!!] =
                    Piece(
                        chessSet = state.chessPosition.matrix[state.selected!!.y][state.selected!!.x]?.chessSet,
                        R.drawable.knight_dark, randomPiece
                    )

                PieceType.ROOK -> state.chessPosition.matrix[state.selected?.y!!][state.selected?.x!!] =
                    Piece(
                        chessSet = state.chessPosition.matrix[state.selected!!.y][state.selected!!.x]?.chessSet,
                        R.drawable.rook_dark, randomPiece
                    )
            }
        }
        println(state.chessPosition.matrix[state.selected?.y!!][state.selected?.x!!].toString())
    }


    fun createRandomChessPiece(): PieceType {
        val rand = Random.nextDouble(0.0, 1.0)

        return when {
            rand < 9.0 / 21.0 -> PieceType.PAWN
            rand < (9.0 / 21.0) + (5.0 / 21.0) -> PieceType.KNIGHT
            rand < (9.0 / 21.0) + (5.0 / 21.0) + (4.0 / 21.0) -> PieceType.BISHOP
            rand < (9.0 / 21.0) + (5.0 / 21.0) + (4.0 / 21.0) + (3.0 / 21.0) -> PieceType.ROOK
            else -> PieceType.QUEEN
        }
    }

    private fun gameWin(): Boolean {
        state.gameWon = true
        println("${state.gameWon}")
        return state.gameWon
    }

    init {
        val chessPosition = ChessPosition(MutableList(8) { MutableList(8) { null } })
        chessPosition.matrix[0][0] =
            Piece(chessSet = ChessSet.WHITE, R.drawable.rook_light, PieceType.ROOK)
        chessPosition.matrix[0][1] =
            Piece(chessSet = ChessSet.WHITE, R.drawable.knight_light, PieceType.KNIGHT)
        chessPosition.matrix[0][2] =
            Piece(chessSet = ChessSet.WHITE, R.drawable.bishop_light, PieceType.BISHOP)
        chessPosition.matrix[0][3] =
            Piece(chessSet = ChessSet.WHITE, R.drawable.king_light, PieceType.KING)
        chessPosition.matrix[0][4] =
            Piece(chessSet = ChessSet.WHITE, R.drawable.queen_light, PieceType.QUEEN)
        chessPosition.matrix[0][5] =
            Piece(chessSet = ChessSet.WHITE, R.drawable.bishop_light, PieceType.BISHOP)
        chessPosition.matrix[0][6] =
            Piece(chessSet = ChessSet.WHITE, R.drawable.knight_light, PieceType.KNIGHT)
        chessPosition.matrix[0][7] =
            Piece(chessSet = ChessSet.WHITE, R.drawable.rook_light, PieceType.ROOK)
        for (i in 0..7) {
            chessPosition.matrix[1][i] =
                Piece(chessSet = ChessSet.WHITE, R.drawable.pawn_light, PieceType.PAWN)
        }

        chessPosition.matrix[7][0] =
            Piece(chessSet = ChessSet.BLACK, R.drawable.rook_dark, PieceType.ROOK)
        chessPosition.matrix[7][1] =
            Piece(chessSet = ChessSet.BLACK, R.drawable.knight_dark, PieceType.KNIGHT)
        chessPosition.matrix[7][2] =
            Piece(chessSet = ChessSet.BLACK, R.drawable.bishop_black, PieceType.BISHOP)
        chessPosition.matrix[7][3] =
            Piece(chessSet = ChessSet.BLACK, R.drawable.queen_dark, PieceType.QUEEN)
        chessPosition.matrix[7][4] =
            Piece(chessSet = ChessSet.BLACK, R.drawable.king_dark, PieceType.KING)
        chessPosition.matrix[7][5] =
            Piece(chessSet = ChessSet.BLACK, R.drawable.bishop_black, PieceType.BISHOP)
        chessPosition.matrix[7][6] =
            Piece(chessSet = ChessSet.BLACK, R.drawable.knight_dark, PieceType.KNIGHT)
        chessPosition.matrix[7][7] =
            Piece(chessSet = ChessSet.BLACK, R.drawable.rook_dark, PieceType.ROOK)
        for (i in 0..7) {
            chessPosition.matrix[6][i] =
                Piece(chessSet = ChessSet.BLACK, R.drawable.pawn_dark, PieceType.PAWN)
        }

        state { copy(chessPosition = chessPosition) }

    }
}