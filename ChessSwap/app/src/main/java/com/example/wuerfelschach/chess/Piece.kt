package com.example.wuerfelschach.chess

import android.graphics.drawable.Icon
import android.media.Image
import androidx.compose.ui.graphics.ImageBitmap
import com.example.wuerfelschach.enums.PieceType
import com.example.wuerfelschach.enums.ChessSet

data class Piece(val chessSet: ChessSet?, var pieceImage: Int?=null, var pieceType: PieceType?=null, )