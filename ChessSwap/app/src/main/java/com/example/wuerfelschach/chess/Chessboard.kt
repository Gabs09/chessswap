package com.example.wuerfelschach.chess

import android.os.Vibrator
import android.os.VibratorManager
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.example.wuerfelschach.R
import com.example.wuerfelschach.enums.ChessSet
import com.example.wuerfelschach.enums.PieceType
import com.example.wuerfelschach.sensor.AcceleratorSensor
import kotlin.math.absoluteValue


@Composable
fun ChessBoard(vm: ChessboardViewModel, navController: NavHostController) {

    val darkSquare = Color.Gray
    val lightSquare = Color.White


    val acceleratorSensor = AcceleratorSensor(LocalContext.current)
    var whiteCounter by remember { mutableStateOf(0) }
    var blackCounter by remember { mutableStateOf(0) }


    var showWinner by remember { mutableStateOf(false) }

    //SENSOR
    acceleratorSensor.setOnSensorValuesChangedListener {
        val x = it[0]
        val y = it[1]
        val z = it[2]

        if ((x > 12 || y > 12 || z > 12) && whiteCounter < 3 && vm.state.whiteOnTurn){
            acceleratorSensor.stopListening()
            vm.updatePiece()
            if (vm.state.whiteOnTurn) whiteCounter++ else blackCounter++
        }
        if ((x > 12 || y > 12 || z > 12) && blackCounter < 5 && !vm.state.whiteOnTurn){

            vm.updatePiece()
            println(vm.state.chessPosition.matrix[vm.state.selected!!.y][vm.state.selected!!.x]?.pieceType.toString())
            acceleratorSensor.stopListening()
            blackCounter++
            Log.d("counter", blackCounter.toString())

        }

    }

    showWinner=vm.state.gameWon
    if (showWinner) {
        WinnerDialog(
            onDismiss = { showWinner = false },
            onMainMenuClick = { navController.navigate("main") }
        )
    }


    //SCHACHBRETT
    Box(
        Modifier
            .fillMaxSize()
            .aspectRatio(1f)
    ) {
        Column {
            for (i in 7 downTo 0) {
                Row(modifier = Modifier.weight(1f)) {
                    for (j in 0..7) {

                        val isLightSquare = i % 2 == j % 2

                        val squareColor = remember {
                            mutableStateOf(if (isLightSquare) lightSquare else darkSquare)
                        }
                        Box(
                            modifier = Modifier

                                .aspectRatio(1f)
                                .background(

                                    color = if (vm.highlightSelected(
                                            Coordinates(i, j)
                                        )
                                    ) Color.Green else squareColor.value
                                )
                                .clickable(
                                    onClick = {
                                        val board = vm.state.chessPosition.matrix
                                        val selected = vm.state.selected
                                        val chessSet =
                                            if (vm.state.whiteOnTurn) ChessSet.WHITE
                                            else ChessSet.BLACK
                                        println(chessSet)
                                        if (selected == null && (board[i][j]
                                                ?.chessSet == chessSet ||
                                                    board[i][j]
                                                        ?.chessSet == chessSet) ||
                                            selected != null && (board[i][j]
                                                ?.chessSet == chessSet ||
                                                    board[i][j]
                                                        ?.chessSet == chessSet)
                                        )
                                            vm.setSelected(Coordinates(i, j))

                                        if (selected != null)
                                            vm.canMove(Coordinates(i, j))
                                        acceleratorSensor.startListening()

                                    },
                                ),
                        ) {
                            vm.state.chessPosition.matrix[i][j]?.pieceImage?.let {
                                painterResource(
                                    id = it
                                )
                            }
                                ?.let { Icon(painter = it, contentDescription = "${vm.state.chessPosition.matrix[i][j]?.pieceType}") }

                        }
                    }
                }
            }
        }
    }
    Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.End) {

    }

}
@Composable
fun WinnerDialog(
    onDismiss: () -> Unit,
    onMainMenuClick: () -> Unit
) {
    Dialog(
        onDismissRequest = { onDismiss() },
        content = {
            WinnerScreen(onMainMenuClick)
        }
    )
}
