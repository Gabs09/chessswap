package com.example.wuerfelschach.chess

import com.example.wuerfelschach.chess.Piece

data class ChessPosition(val matrix: MutableList<MutableList<Piece?>> )