package com.example.wuerfelschach

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material3.Surface
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.wuerfelschach.chess.ChessBoard
import com.example.wuerfelschach.chess.MainScreen
import com.example.wuerfelschach.ui.theme.WuerfelSchachTheme


class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            WuerfelSchachTheme {

                Surface() {
                    val navController = rememberNavController()

                    NavHost(navController = navController, startDestination = "main") {

                        composable(route = "main") {
                            MainScreen(navController = navController)
                        }
/*                        composable(route = "login") {
                            LoginScreen(navController = navController)
                        }
                        composable(route = "signUp") {
                            SignUpScreen(navController = navController)
                        }*/
                        composable(route = "chess") {
                            ChessBoard(viewModel(), navController = navController)
                        }
                    }
                }
            }
        }
    }
}

