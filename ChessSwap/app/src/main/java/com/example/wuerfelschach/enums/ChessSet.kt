package com.example.wuerfelschach.enums

enum class ChessSet {
    WHITE,
    BLACK;
}